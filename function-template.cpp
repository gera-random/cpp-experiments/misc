#include <iostream>
#include <optional>

template<typename T, size_t size>
std::optional<size_t> find(const T &value, const T (&array)[size]) {
    for (size_t i = 0; i < size; ++i) {
        if (array[i] == value)
            return i;
    }
    return {}; // or return std::nullopt;
}

int main() {
    int valueToFind = 4;

    constexpr size_t nElements = 3;
    float array[nElements] = {2, 1, 4};

    auto result = find<float>(valueToFind, array);

    if (result.has_value())
        std::cout << "value " << valueToFind << " was found at index " << result.value() << "\n";
    else
        std::cout << "value " << valueToFind << " was not found\n";

    return 0;
}
