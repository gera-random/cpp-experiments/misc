#include <iostream>
#include <vector>
#include <cmath>
#include <iomanip>

int main() {
    const int nX = 6;
    const int nY = 8;

    std::vector<std::vector<int>> spiral(nY, std::vector<int>(nX, 0));

    int ix = 0;
    int iy = 0;

    int dx = 1;
    int dy = 0;

    int lX = nX - 1;
    int lY = nY - 1;
    int length = lX;
    int steps = 0;

    for (int n = 1; n <= nX * nY; ++n) {
        spiral[iy][ix] = n;

        if (steps != 0 && steps % length == 0) {
            steps = 0;
            if (dx == 1 && dy == 0) {
                dx = 0;
                dy = 1;
                length = lY;
                lY -= 1;
            } else if (dx == 0 && dy == 1) {
                dx = -1;
                dy = 0;
                length = lX;
                lX -= 1;
            } else if (dx == -1 && dy == 0) {
                dx = 0;
                dy = -1;
                length = lY;
                lY -= 1;
            } else if (dx == 0 && dy == -1) {
                dx = 1;
                dy = 0;
                length = lX;
                lX -= 1;
            }
        }
        ++steps;

        ix += dx;
        iy += dy;
    }

    int maxDigits = std::floor(std::log10(nX * nY)) + 2;
    for (const auto & line : spiral) {
        for (int number : line) {
            std::cout << std::setfill(' ') << std::setw(maxDigits) << number;
        }
        std::cout << "\n";
    }
    return 0;
}
