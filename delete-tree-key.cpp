#include <iostream>
#include <fstream>
#include <optional>
#include <functional>

struct Node {
    Node *left;
    Node *right;
    int value;

    bool hasLeft() { return left != nullptr; }

    bool hasRight() { return right != nullptr; }

    Node(int value) : left(nullptr), right(nullptr), value(value) {}
};

class BinTree {
public:
    BinTree() : mRoot(nullptr) {}

    void insert(int value);

    void traverseLeft(std::function<void(int)> action);

private:
    void insert(int value, Node *place);

    void traverseLeft(std::function<void(int)> action, Node *place);

    Node *mRoot;
};

void BinTree::insert(int value) {
    if (mRoot) {
        insert(value, mRoot);
    } else {
        mRoot = new Node(value);
    }
}

void BinTree::insert(int value, Node *place) {
    if (value < place->value) {
        if (place->hasLeft())
            insert(value, place->left);
        else
            place->left = new Node(value);
    } else if (value > place->value) {
        if (place->hasRight())
            insert(value, place->right);
        else
            place->right = new Node(value);
    }
}

void BinTree::traverseLeft(std::function<void(int)> action) {
    traverseLeft(action, mRoot);
}

void BinTree::traverseLeft(std::function<void(int)> action, Node *place) {
    if (place != nullptr) {
        action(place->value);
        traverseLeft(action, place->left);
        traverseLeft(action, place->right);
    }

}

int main() {
    std::ifstream in("input.txt");
    std::ofstream out("output.txt");

    BinTree tree;

    int tmp;
    while (in >> tmp) {
        tree.insert(tmp);
    }

    tree.traverseLeft([&out](int value) {out << value << "\n";});

    return 0;
}
