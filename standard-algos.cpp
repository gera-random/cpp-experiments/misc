#include <iostream>
#include <vector>
#include <functional>
#include <numeric>
#include <queue>

template<typename T>
void printVector(const std::vector<T> &vec) {
    for (const auto &element : vec) {
        std::cout << element << " ";
    }
    std::cout << "\n";
}

template<typename T>
void printQueue(std::queue<T> q) {
    while (!q.empty()) {
        std::cout << q.front() << " ";
        q.pop();
    }
    std::cout << "\n";
}

int main() {
    std::vector<int> numbers{3, 9, 7, 6};

    std::cout << "Initial vector\n";
    printVector(numbers);

    // using the standard comparison function object
    std::sort(std::begin(numbers), std::end(numbers), std::greater<>());

    std::cout << "Sorted descending\n";
    printVector(numbers);

    // using raw pointers as iterators and lambda as predicate
    std::sort(numbers.data(), numbers.data() + numbers.size(), [](int a, int b) { return a < b; });

    std::cout << "Sorted ascending\n";
    printVector(numbers);

    auto greaterOrEqualThanFive = std::bind(std::greater_equal<>(), std::placeholders::_1, 5);
    // reverse iterators work as well
    auto nGreaterThanFive = std::count_if(std::crbegin(numbers), std::crend(numbers), greaterOrEqualThanFive);

    std::cout << nGreaterThanFive << " numbers are ≥ five\n";

    std::function<int(int, int)> intXorInt = [](int a, int b) -> int { return a ^ b; };
    //auto elementsXor = std::accumulate(++std::cbegin(numbers), std::cend(numbers), *std::cbegin(numbers), intXorInt);
    // 0 xor smth is smth, therefore 0 works as identity for xor
    auto elementsXor = std::accumulate(std::cbegin(numbers), std::cend(numbers), 0, intXorInt);

    std::cout << "Xor of all elements " << elementsXor << "\n";

    std::cout << "-\t-\t-\t-\t-\t-\t-\n";

    numbers.assign({5, 6, 8, 4, 4, 4, 4, 9, 3});
    std::cout << "Assigned new elements to the vector \n";
    printVector(numbers);

    std::partition(std::begin(numbers), std::end(numbers), greaterOrEqualThanFive);
    auto partPointIter = std::partition_point(std::begin(numbers), std::end(numbers), greaterOrEqualThanFive);

    std::cout << "Elements ≥ 5 are now on the left\nPartition point is " << *partPointIter << "\n";

    printVector(numbers);

    // reverse iterators and greater comparison result in ascending order
    std::stable_sort(std::rbegin(numbers), std::rend(numbers), std::greater<>());

    std::cout << "Vector is once again sorted\n";

    printVector(numbers);

    std::cout << "Binary searching for elements 0-9\n";
    std::queue<int> elementsFoundByBinSearch;
    for (int i = 0; i < 10; ++i) {
        auto searchResult = std::binary_search(numbers.cbegin(), numbers.cend(), i);
        if (searchResult)
            elementsFoundByBinSearch.push(i);
    }

    std::cout << "Elements found by binary search\n";
    printQueue(elementsFoundByBinSearch);

    std::string needle = "fox";
    std::string haystack = "The quick brown fox jumped over the lazy dog.";
    std::boyer_moore_searcher searcher(std::cbegin(needle), std::cend(needle));
    auto searchResult = std::search(std::cbegin(haystack), std::cend(haystack), searcher);
    std::cout << (searchResult != haystack.end() ? "needle found" : "needle not found") << "\n";

    std::cout << "-\t-\t-\t-\t-\t-\t-\n";

    std::sort(std::begin(numbers), std::end(numbers));
    std::vector otherNumbers{2,3,5,1,7};
    std::sort(std::begin(otherNumbers), std::end(otherNumbers));

    std::cout << "Now we have two sorted vectors:\n";
    std::cout << "A = "; printVector(numbers);
    std::cout << "B = "; printVector(otherNumbers);

    std::vector<int> resultVector(std::min(numbers.size(), otherNumbers.size()));
    auto intersectionResult = std::set_intersection(std::cbegin(numbers), std::cend(numbers), std::cbegin(otherNumbers),
            std::cend(otherNumbers), std::begin(resultVector));
    resultVector.erase(intersectionResult, std::cend(resultVector));
    std::cout << "A ∩ B = "; printVector(resultVector);

    resultVector.assign(numbers.size() + otherNumbers.size(), 0);
    std::merge(std::cbegin(numbers), std::cend(numbers), std::cbegin(otherNumbers), std::cend(otherNumbers),
            std::begin(resultVector));
    std::cout << "Merged A and B\n";
    printVector(resultVector);

    otherNumbers.resize(numbers.size());
    std::move(std::cbegin(numbers), std::cend(numbers), std::begin(otherNumbers));
    std::cout << "Moved A into B:\nB = ";
    printVector(otherNumbers);
    // numbers is in valid, but otherwise indeterminate state

    return 0;
}
